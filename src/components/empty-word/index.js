import React from 'react';

import 'twin.macro';

function EmptyWord() {
  const [lenght, setLenght] = React.useState(0);

  return (
    <div tw="ml-8">
      <label htmlFor="lenght">Palabra de
        <input
          tw="mx-1 border-dotted w-12 text-2xl text-center"
          id="lenght"
          type="number"
          min="0"
          onChange={({target}) => setLenght(target.value)}
        />letras:
      </label>
      <ul tw="flex">
        {Array(Number(lenght))
            .fill()
            .map((_, index) => (
              <li key={`position-${index}`} tw="list-none">
                <input
                  type="text"
                  tw="mx-4 border-dotted w-16 text-6xl text-center"
                />
              </li>
            ))}
      </ul>
    </div>
  );
}

export default EmptyWord;
