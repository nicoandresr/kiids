import PropTypes from 'prop-types';
import React from 'react';

import 'twin.macro';

const OFFSET = 8;

const pointer = ({clientX, clientY}) =>
  ({x: clientX - OFFSET, y: clientY - OFFSET});

const propTypes = {
  id: PropTypes.string.isRequired,
  ctx: PropTypes.object.isRequired,
};

const Canvas = ({ctx, id}) => {
  let isDrawn = false;

  function handleDraw(event) {
    if (!isDrawn) return;

    const {x, y} = pointer(event);

    ctx.lineTo(x, y);
    ctx.stroke();
  }

  function handleDown(event) {
    const {x, y} = pointer(event);
    ctx.beginPath();
    ctx.moveTo(x, y);
    isDrawn = true;
  }

  return (
    <canvas
      tw="border-4 border-dashed"
      id={id}
      onMouseMove={handleDraw}
      onPointerDown={handleDown}
      onPointerUp={() => isDrawn = false}
      height="700"
      width="900"
    />
  );
};

Canvas.propTypes = propTypes;

function useCanvas(canvasId) {
  const [canvas, setCanvas] = React.useState({});

  React.useEffect(() => {
    const target = document.getElementById(canvasId);
    const context = target.getContext('2d');
    setCanvas({
      context,
      clear: () => context.clearRect(0, 0, target.width, target.height),
    });
  }, []);

  return canvas;
}

const DrawCanvas = () => {
  const DRAW_CANVAS = 'draw-canvas';
  const {context, clear} = useCanvas(DRAW_CANVAS);

  return (
    <>
      <Canvas ctx={context} id={DRAW_CANVAS} />

      <button
        type="button"
        onClick={clear}
        tw="h-8 ml-4"
      >
        Borrar
      </button>
    </>
  );
};

export default DrawCanvas;
