import DrawCanvas from '/components/draw-canvas';
import EmptyWord from '/components/empty-word';
import React from 'react';
import ReactDOM from 'react-dom';

import 'twin.macro';

function App() {
  return (
    <div tw="flex w-full">
      <DrawCanvas />

      <EmptyWord />
    </div>
  );
}

const rootElement = document.getElementById('app');
ReactDOM.render(<App />, rootElement);
